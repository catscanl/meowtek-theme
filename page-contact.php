<?php

/*
Template Name: Contact Page
*/

get_header(); ?>

	<?php include(TEMPLATEPATH . "/sidebar-support.php"); ?>
	
	<div id="primary" class="site-content">
		<div id="content content-support" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
				<?php comments_template( '', true ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<div id="sidebar-support">
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-support') ) ?>
</div>

<?php get_footer(); ?>