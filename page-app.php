<?php

/*
Template Name: App Page
*/

get_header(); ?>
	
	<div id="primary" class="site-content">
		<div id="content content-support" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
				<?php comments_template( '', true ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<div id="sidebar-app">
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-app') ) ?>
</div>

<?php get_footer(); ?>