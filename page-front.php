<?php

/*
Template Name: Front Page Cust
*/

get_header(); ?>

	<?php include(TEMPLATEPATH . "/sidebar-support.php"); ?>
	
	<div id="primary" class="site-content">
		<div id="content content-support" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
				<?php comments_template( '', true ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<div id="sidebar-front">
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-2') ) ?>
</div>

<?php get_footer(); ?>