<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	</div><!-- #main .wrapper -->
		<footer id="colophon" role="contentinfo">
			<div class="footwrapper">
				<div class="footerFloat footerFloatLeft">
					<p>Contact us <a href="/support">here</a></p>
				</div>
				<div class="footerFloat">
					<p>&#169; 2014 Meowtek</p>
				</div>
				<div class="footerFloat footerFloatRight">
					<p>Built by <a href="http://www.cathyscanlon.com">catscanl</p>
				</div>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>