<?php
/**
 * The sidebar containing the main widget area
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div id="secondary" class="widget-area phonzy-side" role="complementary">
			<div class="sidebar-div">
				<h2>Some Great Feedback From Reviewers...</h2>
				
 					<h3><a href="http://teacherswithapps.com/phonics-phonzy">TeacherWithApps</a></h3>
	 				<p>"I want to shout out to Meowtek…for blazing new trails! They are trying to make learning fun!"<p>
	
					<h3><a href="http://www.smartappsforkids.com/2014/11/review-phonics-with-phonzy.html#more">SmartAppsforKids</a></h3>
	 				<p>"Cute, engaging character (even without a leather jacket) that encourages the user to participate with lots of praise and reassurance."<p>
			
					<h3><a href="http://igamemom.com/2014/11/14/free-app-phonics-with-phonzy-practice-letter-sounds-and-words-aloud">igamemom</a></h3>
	 				<p>"It is unique in ways of encouraging kids to practice the letters and words out loud, which is essential in any language learning experience."<p>
			
	 				<h3><a href="http://fun2tap.com/index.cfm?rid=475</a>Fun2tap">Fun2tap</a></h3>
	 				<p>"The 3D animation is very detailed so that kids can clearly see the movements ...This is great for kids who may be struggling with their pronunciation of certain sounds or words."<p>
			</div>

			<div class="sidebar-div">
				<h2>App Screenshots</h2>

				<a href="/wp-content/uploads/2015/01/6073944_orig.png"><img src="/wp-content/uploads/2015/01/6073944_orig.png"  class="sidebar-image" /></a>
				<a href="/wp-content/uploads/2015/01/4682785_orig.png"><img src="/wp-content/uploads/2015/01/4682785_orig.png"  class="sidebar-image" /></a>
				<a href="/wp-content/uploads/2015/01/8328813_orig.png"><img src="/wp-content/uploads/2015/01/8328813_orig.png"  class="sidebar-image" /></a>
				<a href="/wp-content/uploads/2015/01/1729084_orig.png"><img src="/wp-content/uploads/2015/01/1729084_orig.png"  class="sidebar-image" /></a>
				<a href="/wp-content/uploads/2015/01/5852997_orig.png"><img src="/wp-content/uploads/2015/01/5852997_orig.png"  class="sidebar-image" /></a>
			</div>

		</div><!-- #secondary -->
	<?php endif; ?>